void foo(OilTank t)
{
  // ...
}

OilTank t(50, 100);

foo(t); // Copy constructor is called

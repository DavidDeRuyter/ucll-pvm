class Person
{
private:
  int age;

public:
  int getAge();
};

int `\NODE{Person::}{class designator}`getAge()
{
  return age;
}

#include "bitmap.h"
#include "formats.h"
#include "filter.h"
#include "color_filters.h"

int main(int argc, char** argv)
{
	Bitmap bm = load_bitmap("C:/PvM/sample.bmp");
	//red filter
	RedFilter rf;
	auto bm2 = rf.apply(bm);
	save_bitmap("C:/PVM/01.bmp", *bm2);
	//green filter
	GreenFilter gf;
	auto bm3 = gf.apply(bm);
	save_bitmap("C:/PVM/02.bmp", *bm3);
	//blue  filter
	BlueFilter bf;
	auto bm4 = bf.apply(bm);
	save_bitmap("C:/PVM/03.bmp", *bm4);
	//blue  filter
	GreyFilter grf;
	auto bm5 = grf.apply(bm);
	save_bitmap("C:/PVM/04.bmp", *bm5);
	return 0;
}
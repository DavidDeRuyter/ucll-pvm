#include "fractials.h"
#include "bitmap.h"
#include "formats.h"
#include "interval.h"
#include "interval_mapper.h"
#include <iostream>
#include <fstream>
using namespace std;

fractials::fractials()
{
}


fractials::~fractials()
{
}

int main() {
	//maak de lege tekening aan (werkt)
	Bitmap bitm = Bitmap(500, 500);
	//maak de kleur rood aan en schrijf de kleur naar de tekening (exercise 2)
	/*
	color redcolor = color(1, 0, 0);
	bitm.clear(redcolor);
	*/
	//maak een interval-mapper aan (exercise 3 boven naar onder)
	/*
	interval from(0, bitm.height() - 1);
	interval to(0, 1);
	interval_mapper mapper(from, to);
	//schrijf de kleur weg
	for (int x = 0; x < bitm.height(); x++) {
		for (int y = 0; y < bitm.height(); y++) {
			color col = color(0, 0, mapper.operator[](x));
			bitm[position(y, x)] = col;
		}
	}
	*/
	//van rood naar blauw (boven naar onder - exercise 4)
	/*
	interval from(0, bitm.height() - 1);
	interval to(0, 1);
	interval_mapper mapper(from, to);
	//schrijf de kleur weg
	for (int x = 0; x < bitm.height(); x++) {
		for (int y = 0; y < bitm.height(); y++) {
			color col = color(mapper.operator[](x), 0, 1- mapper.operator[](x));
			bitm[position(y, (bitm.height() - 1) - x)] = col;
		}
	}
	*/
	//van zwart naar wit (exercise 5)
	/*
	interval from(0, bitm.height() - 1);
	interval to(0, 1);
	interval_mapper mapper(from, to);
	//schrijf de kleur weg
	for (int x = 0; x < bitm.height(); x++) {
		for (int y = 0; y < bitm.height(); y++) {
			color col = color(mapper.operator[](x), mapper.operator[](x), mapper.operator[](x));
			bitm[position(x, y)] = col;
		}
	}
	*/
	//van geel naar roze schuin (exercise 6)
	color yellow = colors::yellow();
	color pink = colors::magenta();
	interval from(0, 1000);
	interval to(0, 1);
	interval_mapper mapper(from, to);
	for (int x = 0; x < bitm.width(); x++) {
		for (int y = 0; y < bitm.height(); y++) {
			double t = mapper[x + y];
			bitm[position(x, y)] = t * pink + (1 - t) * yellow;
		}
	}
	//schrijf de tekening naar de onderstande tekst
	save_bitmap("exercise5.bmp", bitm);
	//sluit het programma af
	return 0;
}
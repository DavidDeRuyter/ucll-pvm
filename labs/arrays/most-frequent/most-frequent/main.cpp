#include <vector>
#include <map>

int most_frequent(const std::vector<int>* ns)
{
	if(ns->size() == 0){
		return -1;
	}
	std::map <int, int> number_list;
	for (int n : *ns) {
		if (number_list.find(n) == number_list.end())
		{
			number_list[n] = 1;
		}
		else {
			number_list[n] += 1;
		}
	}
	int answer = 0;
	for (int n : *ns) {
		if (answer < number_list[n]) {
			answer = n;
		}
	}
	return answer;
}


/*

TESTS

*/

#define CATCH_CONFIG_MAIN
#include "Catch.h"

void test(const std::vector<int>& ns, int expected)
{
    int actual = most_frequent(&ns);

    REQUIRE(expected == actual);
}

#define TEST(expected, ...) TEST_CASE("most_frequent on " #__VA_ARGS__ ", expected=" #expected, "[most_frequent]") { std::vector<int> ns = __VA_ARGS__; test(ns, expected); }

TEST(-1, {})
TEST(0, { 0 })
TEST(1, { 1 })
TEST(2, { 2 })
TEST(1, { 0, 1, 1 })
TEST(0, { 1, 0, 0 })
TEST(1, { 0, 1, 2, 1 })
TEST(3, { 1, 2, 3, 3, 4, 5 })
TEST(3, { 3, 1, 2, 3, 4, 5 })